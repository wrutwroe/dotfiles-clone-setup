#!/bin/sh


# TEST LINE

# Original idea for dotfiles management taken
# from: https://www.atlassian.com/git/tutorials/dotfiles

BACKUPFILE="config-originals-$(date +%Y%m%d).tar"
GIT=$(which git)

git clone --bare https://gitlab.com/wrutwroe/dotfiles.git "$HOME"/.dotfiles.git
#git clone --bare https://Mike_Lewelling@bitbucket.org/mike_lewelling/dotfiles.git "$HOME"/.dotfiles.git
#git clone --bare git@bitbucket.org:/mike_lewelling/dotfiles.git "$HOME"/.dotfiles.git
function dots {
    $GIT --git-dir="$HOME"/.dotfiles.git/ --work-tree="$HOME" "$@"
}
dots checkout >/dev/null 2>&1
if [ $? = 0 ];
then
    echo "Checked out dotfiles."
else
    printf "\nBacking up pre-existing dot files to: %s \n" "$BACKUPFILE"
    tar -cf "$BACKUPFILE" -T /dev/null
    #    dots checkout 2>&1 | egrep "\s+\." | awk {'print $1'} > conflict-configs
    dots checkout 2>&1 | awk -F'\t' {'print $2'} | grep -v "^$" > conflict-configs
    while read LINE;
    do
        tar -rf "$BACKUPFILE" "$LINE"
        rm "$LINE"
    done < conflict-configs
    rm conflict-configs
fi
dots checkout
if [ $? = 0 ];
then
    printf "\ndotfiles checkout successful.\n"
fi
dots config status.showUntrackedFiles no

## Configure Git
printf "\n   ~~~  Setting up Git configuration  ~~~\n"
printf "Please enter your full name: "
read "gName"
printf "\nPlease enter your email address: "
read "gEmail"
printf "\n"
cat << EOF > ~/.gitconfig 2>&1
[core]
    editor = vim
    autocrlf = input
    whitespace = indent-with-non-tab
[init]
    defaultbranch = main
[user]
    name = "$gName"
    email = "$gEmail"
[push]
    autoSetupRemote = true
[merge]
    tool = vimdiff
    conflictstyle = diff3
[mergetool]
    prompt = false
[mergetool "vimdiff"]
    layout = "LOCAL,BASE,REMOTE / MERGED + BASE,LOCAL + BASE,REMOTE + (LOCAL/BASE/REMOTE) , MERGED"
[alias]
    lg = log --all --graph --decorate --color --pretty=format:'%C(dim green)%>(12,trunc)%cr %C(auto)%h%C(auto)%d %Creset%s%C(dim white) - %cN, %ci%C(reset)'
    lgr = log --all --reverse -15 --decorate --color --pretty=format:'%C(dim green)%>(12,trunc)%cr %C(auto)%h%C(auto)%d %Creset%s%C(dim white) - %cN, %ci%C(reset)'
    lg10 = log --graph --color --decorate -10 --format=format:'%C(dim green)%>(12,trunc)%cr %C(auto)%h%C(auto)%d%C(reset)%n                └ %C(white)%s%C(reset) %C(dim white)- %cN, %ci%C(reset)'
    logx = log --all --graph --decorate=short --color --format=format:'%C(blue)%h%C(reset)+%C(dim black)%>(12,trunc)%cr%C(reset)+%C(auto)%d%C(reset)\n   %C(reset)%C(black)└ %s%C(bold blue) (%cN, %ci)%C(reset)'
    xtree = !bash -c '"                                                                             \
        while IFS=+ read -r hash time branch message; do                                            \
            timelength=$(echo \"$time\" | sed -r \"s:[^ ][[]([0-9]{1,2}(;[0-9]{1,2})?)?m::g\");     \
            timelength=$(echo \"16+${#time}-${#timelength}\" | bc);                                 \
            timelength=12;                                 \
            printf \"%${timelength}s %s %s %26s\n\" \"$time\" \"$hash$branch\" \"$message\";  \
        done < <(git logx && echo);"'
    logt = log --all --graph --decorate=short --color --format=format:'%C(blue)%h%C(reset)+%C(dim black)%>(12,trunc)%cr%C(reset)+%C(auto)%d%C(reset)\n   %C(reset)%C(black)└ %s%C(bold blue) (%cN, %ci)%C(reset)'
    ttree = !bash -c '"                                                                             \
        while IFS=+ read -r hash time branch message; do                                            \
            timelength=$(echo \"$time\" | sed -r \"s:[^ ][[]([0-9]{1,2}(;[0-9]{1,2})?)?m::g\");     \
            timelength=$(echo \"16+${#time}-${#timelength}\" | bc);                                 \
            timelength=12;                                 \
            printf \"%${timelength}s %s %s %26s\n\" \"$time\" \"$hash$branch\" \"$message\";  \
        done < <(git logt && echo);"'
EOF

printf "\nGit Global config has been set with the following parameters:\n"
git config --global --list
printf "\n=== Git setup complete ===\n"

grep 'shell-extras.inc' ~/.bashrc
if [ "$?" -gt '0' ];
then
    printf "\nAdd line to the .bashrc file to include \"shell-extras\" file\n"
    printf "\n[[ -f ~/.shell-extras.inc ]] && . ~/.shell-extras.inc\n" >> ~/.bashrc
fi

