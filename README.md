# README #

When run from my $HOME directory, this script clones my dotfiles repo and because it's a "bare" repository, also deploys the files to their various locations. This script also prompts for name and e-mail address to fill in my .gitconfig file.
